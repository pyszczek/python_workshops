#Installation
For our workshop, we will need a Python 3.8 interpreter. Below are some directions on how to check if you already have the interpreter.
##Windows
You can download Python for Windows directly from [python.org](http://python.org). After downloading the file *.msi, open it and follow the instructions. It is important to remember the path of installation – the directory – as we will need this information during the installation of tools.
###Important
During installation you must tick Add python to **PATH** as otherwise it might be problematic.

##Linux or Mac
In order to check our version of Python, enter the following in the command line:

```bash
$ python --version
Python 3.8.0
```

If the python command is not available or the wrong version appears:
###Ubuntu
Enter in the command line:

```bash
sudo apt-get install python3
```

###OS X
Download and install the package for your system version from [python.org](http://python.org).
