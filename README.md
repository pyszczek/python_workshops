# PySzczek Python Basics Workshop 2020

To be able to use mkdocs first you need to install the requirements:
```bash
pip install -r requirements.txt
```

You will have then access to the following commands:
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.
